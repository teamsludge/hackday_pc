from tkinter import *
from datetime import *
import requests

def search(e=0):
    canvas.delete(ALL)
    city = city_entry.get()
    # &units=metric
    url = "http://api.openweathermap.org/data/2.5/weather?q="+city+"&appid=d5668643f59c3b25a9cc577da78beb4e&units=metric"
    data = requests.get(url).json()
    print (data)
    temp = str(data['main']['temp']) + " °C"
    city = data['name']
    cntr = data['sys']['country']
    wnd_spd = data['wind']['speed']
    dt_time = datetime.fromtimestamp(data['sys']['sunrise'])
    canvas.create_text(150, 50, text=city + ', ' + cntr, fill='white', font=('Comic Sans MS', 40))
    canvas.create_text(150, 100, text=temp, fill='white', font=('Comic Sans MS', 40))
    canvas.create_text(150, 150, text = "WS", fill = 'blue', font = ('Times New Roman', 50))
    canvas.create_text(150, 200, text = str(wnd_spd) + 'm/s', fill = 'blue', font = ('Times New Roman',60))
    canvas.create_text(150, 250, text = "sunrise at " + dt_time.strftime('%H:%M'), fill = 'black', font =('Comis Sans MS', 20))
    dt_time = datetime.fromtimestamp(data['sys']['sunset'])
    canvas.create_text(150, 300, text = "sunset at " + dt_time.strftime('%H:%M'), fill = 'black', font = ('Comis Sans MS', 20))
    icon_name = data['weather'][0]['icon']
    icon = PhotoImage(file = 'weatherimages/' + icon_name + '.gif')
    icon_label.configure(image = icon)
    icon_label.safasdf = icon

window = Tk()
search_frame = Frame(window)
search_frame.grid(column=1, row=1)

city_label = Label(search_frame, text="City:")
city_label.grid(row=1, column=1)

city_entry = Entry(search_frame)
city_entry.grid(row=1, column=2)
city_entry.bind('<Return>', search)

city_button = Button(search_frame, text="Search", command=search)
city_button.grid(row=1, column=3)


result_frame = Frame(window)
result_frame.grid(row=2, column=1)

canvas = Canvas(result_frame, height=500, width=300, bg='#aa2341')
canvas.grid(row=1, column=1)

icon_label = Label(result_frame)
icon_label.grid(row = 2, column = 1)

