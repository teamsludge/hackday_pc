import requests
import sys

# Достаёт инфу о пользователе, конкретно:
# Город, подтверждение человека
def User(uid):
    url = "https://api.vk.com/method/users.get?user_id=" + uid + "&v=5.8&fields=last_seen,counters,followers_count,personal,music"
    data = requests.get(url).json()
    print(data)

def Search(first_name,last_name):
    url_first_name = "https://api.vk.com/method/users.search?q=" + first_name
    data_first_name = requests.get(url_first_name).json()
    url_last_name = "https://api.vk.com/method/users.search?q=" + last_name
    data_last_name = requests.get(url_last_name).json()